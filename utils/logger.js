const { resolve } = require('path');

const winston = require('winston');
require('winston-daily-rotate-file');

const {
  compose,
  mergeDeepRight,
} = require('ramda');

const {
  format: { json },
  transports: { Console, DailyRotateFile },
} = winston;

const defaultOptions = {
  service: 'app',
  level: 'info',
  file: {
    maxSize: '20m',
    maxFiles: '14d',
    directory: resolve(__dirname, '..', 'logs'),
  },
};

const createLogger = (options = defaultOptions) => winston.createLogger({
  level: options.level,
  format: json(),
  defaultMeta: { service: options.service },
  transports: [
    new Console({
      level: options.level,
    }),
    new DailyRotateFile({
      level: options.level,
      zippedArchive: true,
      filename: `${options.service}-%DATE%.log`,
      dirname: options.file.directory,
      maxSize: options.file.maxSize,
      maxFiles: options.file.maxFiles,
    }),
  ],
});

const prepareLoggerOptions = mergeDeepRight(defaultOptions);

module.exports = compose(
  createLogger,
  prepareLoggerOptions,
);
