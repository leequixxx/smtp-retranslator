const config = require('./config');
const createLogger = require('./logger');

module.exports = {
  config,
  createLogger,
};
