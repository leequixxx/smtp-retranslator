const nconf = require('nconf');

module.exports = nconf.env({
  separator: '_',
  lowerCase: true,
});
