const querystring = require('querystring');
const FormData = require('form-data');

const axios = require('axios').default;
const {
  prop,
  path,
} = require('ramda');

const {
  BASE_URL,
  VERSION,
} = require('../const');

const createGetUploadDocumentServer = (accessToken, http = axios) => ({
  peerId,
  type,
}) => {
  const query = querystring.encode({
    v: VERSION,
    access_token: accessToken,
    peer_id: peerId,
    type,
  });

  return http.get(`${BASE_URL}/method/docs.getMessagesUploadServer?${query}`).then(prop('data'));
};

const createUploadDocumentToServer = (http = axios) => (getUploadDocumentServerResponse, {
  filename,
  buffer,
  contentType,
}) => {
  const getUploadUrl = path(['response', 'upload_url']);
  const uploadUrl = getUploadUrl(getUploadDocumentServerResponse);

  const form = new FormData();
  form.append('file', buffer, {
    filename,
    contentType,
  });

  return http.post(uploadUrl, form, { headers: form.getHeaders() }).then(prop('data'));
};

const createSaveDocument = (accessToken, http = axios) => (uploadDocumentToServerResponse, {
  title,
}) => {
  const query = querystring.encode({
    v: VERSION,
    access_token: accessToken,
    title,
    ...uploadDocumentToServerResponse,
  });

  return http.get(`${BASE_URL}/method/docs.save?${query}`).then(prop('data'));
};

const defaultFactories = {
  getUploadDocumentServerFactory: createGetUploadDocumentServer,
  uploadDocumentToServerFactory: createUploadDocumentToServer,
  saveDocumentFactory: createSaveDocument,
};
const createAttachDocument = (accessToken, {
  getUploadDocumentServerFactory,
  uploadDocumentToServerFactory,
  saveDocumentFactory,
} = defaultFactories) => async ({
  peerId,
  file,
  title,
}, type = 'doc') => {
  const getDocument = path(['response', type]);

  const getUploadDocumentServer = getUploadDocumentServerFactory(accessToken);
  const uploadDocumentToServer = uploadDocumentToServerFactory();
  const saveDocument = saveDocumentFactory(accessToken);

  const getUploadDocumentServerResponse = await getUploadDocumentServer({
    peerId,
    type,
  });
  const uploadDocumentToServerResponse = await uploadDocumentToServer(
    getUploadDocumentServerResponse,
    file,
  );
  const saveDocumentResponse = await saveDocument(
    uploadDocumentToServerResponse,
    {
      title,
    },
  );

  const document = getDocument(saveDocumentResponse);

  return `doc${document.owner_id}_${document.id}`;
};

module.exports = {
  createGetUploadDocumentServer,
  createUploadDocumentToServer,
  createSaveDocument,
  createAttachDocument,
};
