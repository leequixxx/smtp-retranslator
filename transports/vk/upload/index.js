const { createAttachDocument } = require('./document');
const { createAttachImage } = require('./image');

module.exports = {
  createAttachDocument,
  createAttachImage,
};
