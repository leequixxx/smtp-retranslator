const querystring = require('querystring');
const FormData = require('form-data');

const axios = require('axios').default;
const {
  prop,
  path,
} = require('ramda');

const {
  BASE_URL,
  VERSION,
} = require('../const');

const createGetUploadImageServer = (accessToken, http = axios) => ({
  peerId,
}) => {
  const query = querystring.encode({
    v: VERSION,
    access_token: accessToken,
    peer_id: peerId,
  });

  return http.get(`${BASE_URL}/method/photos.getMessagesUploadServer?${query}`).then(prop('data'));
};

const createUploadImageToServer = (http = axios) => (getUploadImageServerResponse, {
  filename,
  buffer,
  contentType,
}) => {
  const getUploadUrl = path(['response', 'upload_url']);
  const uploadUrl = getUploadUrl(getUploadImageServerResponse);

  const form = new FormData();
  form.append('photo', buffer, {
    filename,
    contentType,
  });

  return http.post(uploadUrl, form, { headers: form.getHeaders() }).then(prop('data'));
};

const createSaveImage = (accessToken, http = axios) => (uploadImageToServerResponse) => {
  const query = querystring.encode({
    v: VERSION,
    access_token: accessToken,
    ...uploadImageToServerResponse,
  });

  return http.get(`${BASE_URL}/method/photos.saveMessagesPhoto?${query}`).then(prop('data'));
};

const defaultFactories = {
  getUploadImageServerFactory: createGetUploadImageServer,
  uploadImageToServerFactory: createUploadImageToServer,
  saveImageFactory: createSaveImage,
};
const createAttachImage = (accessToken, {
  getUploadImageServerFactory,
  uploadImageToServerFactory,
  saveImageFactory,
} = defaultFactories) => async ({
  peerId,
  file,
}) => {
  const getImage = path(['response', 0]);

  const getUploadImageServer = getUploadImageServerFactory(accessToken);
  const uploadImageToServer = uploadImageToServerFactory();
  const saveImage = saveImageFactory(accessToken);

  const getUploadImageServerResponse = await getUploadImageServer({ peerId });
  const uploadImageToServerResponse = await uploadImageToServer(getUploadImageServerResponse, file);
  const saveImageResponse = await saveImage(uploadImageToServerResponse);

  const image = getImage(saveImageResponse);

  return `photo${image.owner_id}_${image.id}`;
};

module.exports = {
  createGetUploadImageServer,
  createUploadImageToServer,
  createSaveImage,
  createAttachImage,
};
