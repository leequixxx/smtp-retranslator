const createProvideToken = (accessToken) => (factory) => factory(accessToken);

module.exports = { createProvideToken };
