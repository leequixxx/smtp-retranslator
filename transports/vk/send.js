const querystring = require('querystring');

const axios = require('axios').default;
const { prop } = require('ramda');

const {
  BASE_URL,
  VERSION,
} = require('./const');

const createSendMessage = (accessToken, http = axios) => ({
  peerId,
  title,
  message,
  attachment,
}) => {
  const query = querystring.encode({
    v: VERSION,
    access_token: accessToken,
    peer_id: peerId,
    random_id: Date.now(),
    title,
    message,
    attachment,
  });
  return http.get(`${BASE_URL}/method/messages.send?${query}`).then(prop('data'));
};

module.exports = { createSendMessage };
