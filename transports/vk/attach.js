const {
  createAttachImage,
  createAttachDocument,
} = require('./upload');

const defaultFactories = {
  attachImageFactory: createAttachImage,
  attachDocumentFactory: createAttachDocument,
};
const createAttach = (accessToken, {
  attachImageFactory,
  attachDocumentFactory,
} = defaultFactories) => ({ peerId, attachment }) => {
  let attachFile;
  if (attachment.contentType.startsWith('image/')) {
    attachFile = attachImageFactory(accessToken);
  } else {
    attachFile = attachDocumentFactory(accessToken);
  }

  let type = 'doc';
  if (attachment.contentType.startsWith('audio/')) {
    type = 'audio_message';
  }

  return attachFile({
    peerId,
    file: {
      filename: attachment.filename,
      buffer: Buffer.from(attachment.data),
      contentType: attachment.contentType,
    },
  }, type);
};

module.exports = { createAttach };
