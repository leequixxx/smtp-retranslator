const { SMTPServer } = require('smtp-server');

const { config, createLogger } = require('./utils');
const { createProvideToken } = require('./transports/vk/token');
const { createRcptAction, createDataAction } = require('./actions');

const loggerApp = createLogger({
  level: config.get('app:logger:level') || 'info',
});
const loggerSmtp = createLogger({
  service: 'smtp',
  level: config.get('app:smtp:logger:level') || 'debug',
});

const provideToken = createProvideToken(config.get('app:vk:access:token'));
const peerId = config.get('app:vk:peer:id');

const rcpt = createRcptAction({
  domain: config.get('app:smtp:domain') || null,
});
const data = createDataAction({
  provideToken,
  peerId,
});

const server = new SMTPServer({
  logger: loggerSmtp,
  authOptional: true,

  onRcptTo: rcpt,
  onData: data,
});


const host = config.get('app:smtp:host') || '127.0.0.1';
const port = config.get('app:smtp:port') || 25;

server.listen(
  port,
  host,
  () => {
    loggerApp.info('SMTP server is started up', {
      host,
      port,
    });
  },
);
