const {
  compose,
  endsWith,
  mergeDeepRight,
} = require('ramda');

const defaultOptions = {
  domain: null,
};

const createRcptAction = (options = defaultOptions) => ({ address }, session, callback) => {
  const isAvailableEmail = endsWith(`@${options.domain}`);

  if (!options.domain || isAvailableEmail(address)) {
    return callback();
  }
  return callback(new Error(`Address ${address} is not allowed`));
};

const prepareRcptActionOptions = mergeDeepRight(defaultOptions);

module.exports = compose(
  createRcptAction,
  prepareRcptActionOptions,
);
