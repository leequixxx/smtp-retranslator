const {
  compose,
  mergeDeepRight,
} = require('ramda');
const { simpleParser } = require('mailparser');
const htmlToText = require('html-to-text');

const { createAttach } = require('../transports/vk/attach');
const { createSendMessage } = require('../transports/vk/send');

const defaultOptions = {
  peerId: 200000001,
  provideToken: (x) => x,
};

const createDataAction = ({
  peerId,
  provideToken,
} = defaultOptions) => async (stream, _, callback) => {
  const email = await simpleParser(stream);

  const attach = provideToken(createAttach);
  const sendMessage = provideToken(createSendMessage);

  const {
    from,
    text,
    html,
    subject,
    attachments,
  } = email;

  let transportAttachments = [];
  if (attachments && attachments.length) {
    transportAttachments = await Promise.all(attachments
      .filter((attachment) => attachment.type === 'attachment')
      .map((attachment) => attach({
        peerId,
        attachment: {
          filename: attachment.filename,
          data: attachment.content,
          contentType: attachment.contentType,
        },
      })));
  }
  const attachment = transportAttachments.join(',');

  const textSubject = `Тема: ${subject || 'Без названия'}`;
  const textFrom = `От: ${from ? from.text : '<Неизвестно>'}\n`;
  const message = `#почтальон\n${textSubject}\n${textFrom}\n${text || htmlToText.fromString(html, { preserveNewlines: true })}`;

  await sendMessage({
    peerId,
    message,
    attachment,
  });

  callback();
};

const prepareDataActionOptions = mergeDeepRight(defaultOptions);

module.exports = compose(
  createDataAction,
  prepareDataActionOptions,
);
