const createRcptAction = require('./rcpt');
const createDataAction = require('./data');

module.exports = {
  createRcptAction,
  createDataAction,
};
