const { createAttach } = require('../../../transports/vk/attach');

describe('transports', () => {
  describe('vk', () => {
    describe('attach', () => {
      const TOKEN = 'token';
      const ATTACHMENT_IMAGE = 'photo0000_0000';
      const ATTACHMENT_DOCUMENT = 'doc0000_0000';
      const PEER_ID = 12345678;
      const FILENAME = 'test.txt';
      const DATA = [0x1, 0x2, 0x3, 0x4];
      const CONTENT_TYPE_IMAGE = 'image/jpeg';
      const CONTENT_TYPE_TEXT = 'text/plain';
      const CONTENT_TYPE_AUDIO = 'audio/mpeg';

      it('execute attachImage function when got image file', async () => {
        const mockAttachImage = jest.fn(() => Promise.resolve(ATTACHMENT_IMAGE));
        const mockAttachDocument = jest.fn(() => Promise.resolve(ATTACHMENT_DOCUMENT));

        const mockAttach = {
          attachImageFactory: () => mockAttachImage,
          attachDocumentFactory: () => mockAttachDocument,
        };

        const attach = createAttach(TOKEN, mockAttach);

        const result = await attach({
          peerId: PEER_ID,
          attachment: {
            filename: FILENAME,
            data: DATA,
            contentType: CONTENT_TYPE_IMAGE,
          },
        });

        expect(mockAttachImage.mock.calls.length).toBe(1);
        expect(mockAttachDocument.mock.calls.length).toBe(0);

        expect(mockAttachImage.mock.calls[0][0]).toStrictEqual({
          peerId: PEER_ID,
          file: {
            filename: FILENAME,
            buffer: Buffer.from(DATA),
            contentType: CONTENT_TYPE_IMAGE,
          },
        });

        expect(result).toEqual(ATTACHMENT_IMAGE);
      });

      it('execute attachDocument function with type = audio_message, when got audio file', async () => {
        const mockAttachImage = jest.fn(() => Promise.resolve(ATTACHMENT_IMAGE));
        const mockAttachDocument = jest.fn(() => Promise.resolve(ATTACHMENT_DOCUMENT));

        const mockAttach = {
          attachImageFactory: () => mockAttachImage,
          attachDocumentFactory: () => mockAttachDocument,
        };

        const attach = createAttach(TOKEN, mockAttach);

        const result = await attach({
          peerId: PEER_ID,
          attachment: {
            filename: FILENAME,
            data: DATA,
            contentType: CONTENT_TYPE_AUDIO,
          },
        });

        expect(mockAttachImage.mock.calls.length).toBe(0);
        expect(mockAttachDocument.mock.calls.length).toBe(1);

        expect(mockAttachDocument.mock.calls[0][0]).toStrictEqual({
          peerId: PEER_ID,
          file: {
            filename: FILENAME,
            buffer: Buffer.from(DATA),
            contentType: CONTENT_TYPE_AUDIO,
          },
        });
        expect(mockAttachDocument.mock.calls[0][1]).toEqual('audio_message');

        expect(result).toEqual(ATTACHMENT_DOCUMENT);
      });

      it('execute attachDocument function with type = doc, when got another file', async () => {
        const mockAttachImage = jest.fn(() => Promise.resolve(ATTACHMENT_IMAGE));
        const mockAttachDocument = jest.fn(() => Promise.resolve(ATTACHMENT_DOCUMENT));

        const mockAttach = {
          attachImageFactory: () => mockAttachImage,
          attachDocumentFactory: () => mockAttachDocument,
        };

        const attach = createAttach(TOKEN, mockAttach);

        const result = await attach({
          peerId: PEER_ID,
          attachment: {
            filename: FILENAME,
            data: DATA,
            contentType: CONTENT_TYPE_TEXT,
          },
        });

        expect(mockAttachImage.mock.calls.length).toBe(0);
        expect(mockAttachDocument.mock.calls.length).toBe(1);

        expect(mockAttachDocument.mock.calls[0][0]).toStrictEqual({
          peerId: PEER_ID,
          file: {
            filename: FILENAME,
            buffer: Buffer.from(DATA),
            contentType: CONTENT_TYPE_TEXT,
          },
        });
        expect(mockAttachDocument.mock.calls[0][1]).toEqual('doc');

        expect(result).toEqual(ATTACHMENT_DOCUMENT);
      });
    });
  });
});
