const { createSendMessage } = require('../../../transports/vk/send');
const {
  BASE_URL,
  VERSION,
} = require('../../../transports/vk/const');

describe('transports', () => {
  describe('vk', () => {
    describe('send', () => {
      const TOKEN = 'token';
      const PEER_ID = 12341;
      const TITLE = 'title';
      const MESSAGE = 'Hello, world!';
      const ATTACHMENT = 'photo1337';
      const RESPONSE = { response: 64 };

      it('creating http get request with special params', async () => {
        const now = Date.now();

        Date.now = jest.fn(() => now);
        const mockHttp = {
          get: jest.fn(() => Promise.resolve({
            data: RESPONSE,
          })),
        };

        const sendMessage = createSendMessage(TOKEN, mockHttp);

        const result = await sendMessage({
          peerId: PEER_ID,
          title: TITLE,
          message: MESSAGE,
          attachment: ATTACHMENT,
        });

        expect(mockHttp.get.mock.calls.length)
          .toBe(1);
        expect(mockHttp.get.mock.calls[0][0])
          .toBe(`${BASE_URL}/method/messages.send?v=${VERSION}&access_token=${TOKEN}&peer_id=${PEER_ID}&random_id=${now}&title=${TITLE}&message=${encodeURIComponent(MESSAGE)}&attachment=${encodeURI(ATTACHMENT)}`);

        expect(result).toStrictEqual(RESPONSE);
      });
    });
  });
});
