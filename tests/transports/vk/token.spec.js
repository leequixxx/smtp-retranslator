const { createProvideToken } = require('../../../transports/vk/token');

describe('transports', () => {
  describe('vk', () => {
    describe('token', () => {
      const TOKEN = 'token';

      it('provide token when wrap some creator', () => {
        const factory = (accessToken) => () => `${accessToken} is valid`;

        const provideToken = createProvideToken(TOKEN);

        const func = provideToken(factory);
        const result = func();

        expect(result).toEqual(`${TOKEN} is valid`);
      });
    });
  });
});
