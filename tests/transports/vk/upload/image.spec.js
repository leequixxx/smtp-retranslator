const querystring = require('querystring');
const FormData = require('form-data');

const {
  createGetUploadImageServer,
  createUploadImageToServer,
  createSaveImage,
  createAttachImage,
} = require('../../../../transports/vk/upload/image');
const {
  BASE_URL,
  VERSION,
} = require('../../../../transports/vk/const');

describe('transports', () => {
  describe('vk', () => {
    describe('upload', () => {
      describe('image', () => {
        const TOKEN = 'token';
        const PEER_ID = 12341;
        const PHOTO = Buffer.from([0x1, 0x2, 0x3]);

        const GET_UPLOAD_IMAGE_SERVER_RESPONSE = {
          response: {
            album_id: -64,
            upload_url: 'https://example.com',
            user_id: 0,
            grou_id: 1,
          },
        };
        const UPLOAD_IMAGE_TO_SERVER_RESPONSE = {
          server: 1337,
          photo: [],
          hash: '9ae8a890000c098777d8709ddaf2',
        };
        const SAVE_IMAGE_RESPONSE = {
          response: [
            {
              id: 457239018,
              album_id: -64,
              owner_id: -190841485,
              user_id: 100,
              text: '',
              date: 1581891285,
              access_key: '42efe59777520882a9',
            },
          ],
        };

        describe('getUploadImageServer', () => {
          it('creating http get request with special params', async () => {
            const mockHttp = {
              get: jest.fn(() => Promise.resolve({
                data: GET_UPLOAD_IMAGE_SERVER_RESPONSE,
              })),
            };

            const getUploadImageServer = createGetUploadImageServer(TOKEN, mockHttp);

            const result = await getUploadImageServer({
              peerId: PEER_ID,
            });

            expect(mockHttp.get.mock.calls.length)
              .toBe(1);
            expect(mockHttp.get.mock.calls[0][0])
              .toBe(`${BASE_URL}/method/photos.getMessagesUploadServer?v=${VERSION}&access_token=${TOKEN}&peer_id=${PEER_ID}`);

            return expect(result).toStrictEqual(GET_UPLOAD_IMAGE_SERVER_RESPONSE);
          });
        });

        describe('uploadImageToServer', () => {
          const REQUIRED_CONTENT_TYPE_HEADER_VALUE = 'multipart/form-data; boundary';

          it('creating http multipart post request with special params and file form field', async () => {
            const mockHttp = {
              post: jest.fn(() => Promise.resolve({
                data: UPLOAD_IMAGE_TO_SERVER_RESPONSE,
              })),
            };

            const uploadImageToServer = createUploadImageToServer(mockHttp);

            const result = await uploadImageToServer(GET_UPLOAD_IMAGE_SERVER_RESPONSE, {
              filename: 'image.jpeg',
              buffer: PHOTO,
              contentType: 'image/jpeg',
            });

            expect(mockHttp.post.mock.calls.length)
              .toBe(1);
            expect(mockHttp.post.mock.calls[0][0])
              .toBe(GET_UPLOAD_IMAGE_SERVER_RESPONSE.response.upload_url);
            expect(mockHttp.post.mock.calls[0][1])
              .toBeInstanceOf(FormData);
            expect(mockHttp.post.mock.calls[0][2].headers['content-type'])
              .toContain(REQUIRED_CONTENT_TYPE_HEADER_VALUE);

            return expect(result).toStrictEqual(UPLOAD_IMAGE_TO_SERVER_RESPONSE);
          });
        });

        describe('saveImage', () => {
          it('creating http get request with special params', async () => {
            const mockHttp = {
              get: jest.fn(() => Promise.resolve({
                data: SAVE_IMAGE_RESPONSE,
              })),
            };

            const saveImage = createSaveImage(TOKEN, mockHttp);

            const result = await saveImage(UPLOAD_IMAGE_TO_SERVER_RESPONSE);

            expect(mockHttp.get.mock.calls.length)
              .toBe(1);
            expect(mockHttp.get.mock.calls[0][0])
              .toBe(`${BASE_URL}/method/photos.saveMessagesPhoto?v=${VERSION}&access_token=${TOKEN}&${querystring.encode(UPLOAD_IMAGE_TO_SERVER_RESPONSE)}`);

            return expect(result).toStrictEqual(SAVE_IMAGE_RESPONSE);
          });
        });

        describe('attachImage', () => {
          it('get upload image server, upload image to server and save image', async () => {
            const mockImage = {
              getUploadImageServerFactory:
                () => jest.fn(() => Promise.resolve(GET_UPLOAD_IMAGE_SERVER_RESPONSE)),
              uploadImageToServerFactory:
                () => jest.fn(() => Promise.resolve(UPLOAD_IMAGE_TO_SERVER_RESPONSE)),
              saveImageFactory:
                () => jest.fn(() => Promise.resolve(SAVE_IMAGE_RESPONSE)),
            };

            const attachImage = createAttachImage(TOKEN, mockImage);

            const result = await attachImage({
              peerId: PEER_ID,
              file: {
                filename: 'image.jpeg',
                buffer: PHOTO,
                contentType: 'image/jpeg',
              },
            });

            const image = SAVE_IMAGE_RESPONSE.response[0];

            return expect(result).toEqual(`photo${image.owner_id}_${image.id}`);
          });
        });
      });
    });
  });
});
