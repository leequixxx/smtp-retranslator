const querystring = require('querystring');
const FormData = require('form-data');

const {
  createGetUploadDocumentServer,
  createUploadDocumentToServer,
  createSaveDocument,
  createAttachDocument,
} = require('../../../../transports/vk/upload/document');

const {
  BASE_URL, VERSION,
} = require('../../../../transports/vk/const');

describe('transports', () => {
  describe('vk', () => {
    describe('upload', () => {
      describe('document', () => {
        const TOKEN = 'token';
        const PEER_ID = 12341;
        const TYPE_DOC = 'doc';
        const TYPE_AUDIO = 'audio_message';
        const TYPES = [TYPE_DOC, TYPE_AUDIO];
        const FILE = Buffer.from([0x1, 0x2, 0x3]);
        const TITLE = 'file';

        const GET_UPLOAD_DOCUMENT_SERVER_RESPONSE = {
          response: {
            upload_url: 'https://example.com',
          },
        };
        const UPLOAD_DOCUMENT_TO_SERVER_RESPONSE = {
          file: '1337|0',
        };
        const SAVE_DOCUMENT_RESPONSE = (type) => ({
          response: {
            type,
            [type]: {
              id: 457239018,
              owner_id: -190841485,
              title: 'file.txt',
              size: 1231234,
              ext: 'txt',
              url: 'https://example.com/files/file.txt',
              date: 1581891285,
              type: 8,
            },
          },
        });

        describe('getUploadDocumentServer', () => {
          it('creating http get request with special params', async () => {
            await Promise.all(TYPES.map(async (type) => {
              const mockHttp = {
                get: jest.fn(() => Promise.resolve({
                  data: GET_UPLOAD_DOCUMENT_SERVER_RESPONSE,
                })),
              };

              const getUploadDocumentServer = createGetUploadDocumentServer(TOKEN, mockHttp);

              const result = await getUploadDocumentServer({
                peerId: PEER_ID,
                type,
              });

              expect(mockHttp.get.mock.calls.length)
                .toBe(1);
              expect(mockHttp.get.mock.calls[0][0])
                .toBe(`${BASE_URL}/method/docs.getMessagesUploadServer?v=${VERSION}&access_token=${TOKEN}&peer_id=${PEER_ID}&type=${type}`);

              return expect(result).toStrictEqual(GET_UPLOAD_DOCUMENT_SERVER_RESPONSE);
            }));
          });
        });

        describe('uploadDocumentToServer', () => {
          const REQUIRED_CONTENT_TYPE_HEADER_VALUE = 'multipart/form-data; boundary';

          it('creating http multipart post request with special params and file form field', async () => {
            const mockHttp = {
              post: jest.fn(() => Promise.resolve({
                data: UPLOAD_DOCUMENT_TO_SERVER_RESPONSE,
              })),
            };

            const uploadDocumentToServer = createUploadDocumentToServer(mockHttp);

            const result = await uploadDocumentToServer(GET_UPLOAD_DOCUMENT_SERVER_RESPONSE, {
              filename: 'file.txt',
              buffer: FILE,
              contentType: 'text/plain',
            });

            expect(mockHttp.post.mock.calls.length)
              .toBe(1);
            expect(mockHttp.post.mock.calls[0][0])
              .toBe(GET_UPLOAD_DOCUMENT_SERVER_RESPONSE.response.upload_url);
            expect(mockHttp.post.mock.calls[0][1])
              .toBeInstanceOf(FormData);
            expect(mockHttp.post.mock.calls[0][2].headers['content-type'])
              .toContain(REQUIRED_CONTENT_TYPE_HEADER_VALUE);

            return expect(result).toStrictEqual(UPLOAD_DOCUMENT_TO_SERVER_RESPONSE);
          });
        });

        describe('saveDocument', () => {
          it('creating http get request with special params', async () => {
            await Promise.all(TYPES.map(async (type) => {
              const mockHttp = {
                get: jest.fn(() => Promise.resolve({
                  data: SAVE_DOCUMENT_RESPONSE(type),
                })),
              };

              const saveDocument = createSaveDocument(TOKEN, mockHttp);

              const result = await saveDocument(
                UPLOAD_DOCUMENT_TO_SERVER_RESPONSE,
                {
                  title: TITLE,
                },
              );

              expect(mockHttp.get.mock.calls.length)
                .toBe(1);
              expect(mockHttp.get.mock.calls[0][0])
                .toBe(`${BASE_URL}/method/docs.save?v=${VERSION}&access_token=${TOKEN}&title=${TITLE}&${querystring.encode(UPLOAD_DOCUMENT_TO_SERVER_RESPONSE)}`);

              return expect(result).toStrictEqual(SAVE_DOCUMENT_RESPONSE(type));
            }));
          });
        });

        describe('attachDocument', () => {
          it('get upload document server, upload document to server and save document', async () => {
            await Promise.all(TYPES.map(async (type) => {
              const mockDocument = {
                getUploadDocumentServerFactory:
                  () => jest.fn(() => Promise.resolve(GET_UPLOAD_DOCUMENT_SERVER_RESPONSE)),
                uploadDocumentToServerFactory:
                  () => jest.fn(() => Promise.resolve(UPLOAD_DOCUMENT_TO_SERVER_RESPONSE)),
                saveDocumentFactory:
                  () => jest.fn(() => Promise.resolve(SAVE_DOCUMENT_RESPONSE(type))),
              };

              const attachDocument = createAttachDocument(TOKEN, mockDocument);

              const result = await attachDocument({
                peerId: PEER_ID,
                file: {
                  filename: 'file.txt',
                  buffer: FILE,
                  contentType: 'text/plain',
                },
                title: TITLE,
              }, type);

              const document = SAVE_DOCUMENT_RESPONSE(type).response[type];

              return expect(result).toEqual(`doc${document.owner_id}_${document.id}`);
            }));
          });
        });
      });
    });
  });
});
