const createRcpt = require('../../actions/rcpt');

describe('actions', () => {
  describe('rcpt', () => {
    describe('when creator got default options', () => {
      it('should execute callback without arguments everytime', () => {
        const mockCallback = jest.fn();

        const rcpt = createRcpt();

        rcpt({ address: 'mail@localhost' }, {}, mockCallback);
        rcpt({ address: 'mail@example.com' }, {}, mockCallback);

        expect(mockCallback.mock.calls.length).toBe(2);
        expect(mockCallback.mock.calls[0].length).toBe(0);
        expect(mockCallback.mock.calls[1].length).toBe(0);
      });
    });
    describe('when creator got domain option', () => {
      it('should execute callback with error argument if, domain is invalid', () => {
        const mockCallback = jest.fn();

        const rcpt = createRcpt({ domain: 'localhost' });

        rcpt({ address: 'mail@example.com' }, {}, mockCallback);

        expect(mockCallback.mock.calls.length).toBe(1);
        expect(mockCallback.mock.calls[0][0]).toBeInstanceOf(Error);
      });

      it('should execute callback without arguments, if domain is valid', () => {
        const mockCallback = jest.fn();

        const rcpt = createRcpt({ domain: 'localhost' });

        rcpt({ address: 'mail@localhost' }, {}, mockCallback);

        expect(mockCallback.mock.calls.length).toBe(1);
        expect(mockCallback.mock.calls[0][0]).toBeUndefined();
      });
    });
  });
});
